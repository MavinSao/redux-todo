import { addTodo, toggleTodo } from './../actions/actionType'
const initState = {
    allIds: [],
    byIds: {}
}

export const todoReducer = (state = initState, action) => {
    switch (action.type) {
        case addTodo: {
            const { id, content } = action.payLoad
            return {
                ...state,
                allIds: [...state.allIds, id],
                byIds: {
                    ...state.byIds,
                    [id]: {
                        content: content,
                        complete: false
                    }
                }
            };
        }
        case toggleTodo: {
            const { id } = action.payLoad;
            return {
                ...state,
                byIds: {
                    ...state.byIds,
                    [id]: {
                        ...state.byIds[id],
                        complete: !state.byIds[id].complete
                    }
                }
            }
        }

        default:
            return state
    }
}