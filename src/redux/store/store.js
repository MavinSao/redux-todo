import { createStore } from 'redux'
import rootReducer from './../reducer/rootReducer'
import { todoReducer } from './../reducer/todoReducer'
const store = createStore(todoReducer)
export default store