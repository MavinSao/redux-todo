import { addTodo, toggleTodo } from './actionType'

let nextTodoId = 0;
export const newTodo = (content) => ({
    type: addTodo,
    payLoad: { id: nextTodoId++, content, complete: false },
})

export const completeTodo = (id) => (
    {
        type: toggleTodo,
        payLoad: { id }
    })