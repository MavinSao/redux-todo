import React, { useState } from "react";
import { newTodo } from "./../redux/actions/action";
import { connect } from "react-redux";
function Input({ newTodo }) {
  const [content, setContent] = useState("");
  return (
    <div>
      <h1>What your main focus today?</h1>
      <input
        type="text"
        name="input"
        id="input"
        onChange={(e) => setContent(e.target.value)}
      />
      <button onClick={() => newTodo(content)}>Add</button>
    </div>
  );
}

const mapDispatchToProps = { newTodo };

export default connect(null, mapDispatchToProps)(Input);
