import React from "react";
import { connect } from "react-redux";
import { completeTodo } from "./../redux/actions/action";
function List({ todos, completeTodo }) {
  return (
    <div>
      <br />
      <ul>
        {todos && todos.length
          ? todos.map((todo, index) => (
              <li
                onClick={() => completeTodo(index)}
                key={index}
                style={
                  todo.complete
                    ? { textDecoration: "line-through", listStyle: "none" }
                    : { listStyle: "none" }
                }
              >
                {todo.content}
              </li>
            ))
          : "No todos, yay!"}
      </ul>
    </div>
  );
}
const getAllTodoById = (data) => {
  return data.allIds.length === 0
    ? []
    : data.allIds.map((id) => data.byIds[id]);
};

const mapStateToProps = (store) => {
  let todos = getAllTodoById(store);
  console.log(todos);

  return { todos };
};
export default connect(mapStateToProps, { completeTodo })(List);
